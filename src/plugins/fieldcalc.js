
//import axios from 'axios';
import store from '@/store';
import axios from 'axios';
//console.log('!!!!!!');
//console.log(axios);
//import jspath from 'jspath';
import JSPath from 'jspath';
//console.log('jspath', JSPath);
var sjspath = JSPath;
window.jspath = sjspath;
window.$axios = axios;
window.$store = store;
//import {parser} from './parser';
//import parser from './parser';
//console.log(parser);
var parser = (function(){
    var o=function(k,v,o,l){for(o=o||{},l=k.length;l--;o[k[l]]=v);return o},$V0=[1,3],$V1=[1,4],$V2=[1,5],$V3=[1,6],$V4=[1,13],$V5=[1,7],$V6=[1,8],$V7=[1,9],$V8=[1,10],$V9=[1,11],$Va=[1,12],$Vb=[1,14],$Vc=[1,15],$Vd=[1,17],$Ve=[1,18],$Vf=[1,19],$Vg=[1,20],$Vh=[1,21],$Vi=[5,6,7,8,9,10,12,19],$Vj=[5,6,7,12,19],$Vk=[5,6,7,8,9,12,19];
    var parser = {trace: function trace () { },
    yy: {},
    symbols_: {"error":2,"expressions":3,"e":4,"EOF":5,"+":6,"-":7,"*":8,"/":9,"^":10,"(":11,")":12,"now":13,"jspath":14,"STRING":15,"loadrest":16,"img_attr":17,"getrefval":18,",":19,"NUMBER":20,"false":21,"true":22,"E":23,"PI":24,"$accept":0,"$end":1},
    terminals_: {2:"error",5:"EOF",6:"+",7:"-",8:"*",9:"/",10:"^",11:"(",12:")",13:"now",14:"jspath",15:"STRING",16:"loadrest",17:"img_attr",18:"getrefval",19:",",20:"NUMBER",21:"false",22:"true",23:"E",24:"PI"},
    productions_: [0,[3,2],[4,3],[4,3],[4,3],[4,3],[4,3],[4,2],[4,3],[4,3],[4,4],[4,4],[4,4],[4,8],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1]],
    performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate /* action[1] */, $$ /* vstack */, _$ /* lstack */) {
    /* this == yyval */
    
    var $0 = $$.length - 1;
    switch (yystate) {
    case 1:
    return $$[$0-1];
    break;
    case 2:
    this.$ = $$[$0-2]+$$[$0];
    break;
    case 3:
    this.$ = $$[$0-2]-$$[$0];
    break;
    case 4:
    this.$ = $$[$0-2]*$$[$0];
    break;
    case 5:
    this.$ = $$[$0-2]/$$[$0];
    break;
    case 6:
    this.$ = Math.pow($$[$0-2], $$[$0]);
    break;
    case 7:
    this.$ = -$$[$0];
    break;
    case 8:
    this.$ = $$[$0-1];
    break;
    case 9:
            
            this.$ = fieldcalc.cdate();
            
    break;
    case 10:
    
    //            if (typeof exports === 'undefined' )
                    this.$ = fieldcalc.getdocvalue($$[$0-1]);
    //            else
    //                this.$ = global.fieldcalc.getdocvalue($$[$0-1]);
            
    break;
    case 11:
    
                if (typeof exports === 'undefined' )
                    this.$ = fieldcalc.loadrest($$[$0-1]);
                else
                    this.$ = global.fieldcalc.loadrest($$[$0-1]);
            
    break;
    case 12:
    
                if (typeof exports === 'undefined' )
                    this.$ = img_attr($$[$0-1]);
                else
                    this.$ = $$[$0-1];
            
    break;
    case 13:
    
                if (typeof exports === 'undefined')
                    this.$ = fieldcalc.getrefval($$[$0-5],$$[$0-3],$$[$0-1]);
                else                
                    this.$ = global.fieldcalc.getrefval($$[$0-5],$$[$0-3],$$[$0-1]);
            
    break;
    case 14:
    this.$ = Number(yytext);
    break;
    case 15:
    this.$ = false;
    break;
    case 16:
    this.$ = true;
    break;
    case 17:
    this.$ = yytext;
    break;
    case 18:
    this.$ = Math.E;
    break;
    case 19:
    this.$ = Math.PI;
    break;
    }
    },
    table: [{3:1,4:2,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{1:[3]},{5:[1,16],6:$Vd,7:$Ve,8:$Vf,9:$Vg,10:$Vh},{4:22,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{4:23,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{11:[1,24]},{11:[1,25]},{11:[1,26]},{11:[1,27]},{11:[1,28]},o($Vi,[2,14]),o($Vi,[2,15]),o($Vi,[2,16]),o($Vi,[2,17]),o($Vi,[2,18]),o($Vi,[2,19]),{1:[2,1]},{4:29,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{4:30,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{4:31,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{4:32,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{4:33,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},o($Vi,[2,7]),{6:$Vd,7:$Ve,8:$Vf,9:$Vg,10:$Vh,12:[1,34]},{12:[1,35]},{15:[1,36]},{4:37,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{4:38,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{4:39,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},o($Vj,[2,2],{8:$Vf,9:$Vg,10:$Vh}),o($Vj,[2,3],{8:$Vf,9:$Vg,10:$Vh}),o($Vk,[2,4],{10:$Vh}),o($Vk,[2,5],{10:$Vh}),o($Vi,[2,6]),o($Vi,[2,8]),o($Vi,[2,9]),{12:[1,40]},{6:$Vd,7:$Ve,8:$Vf,9:$Vg,10:$Vh,12:[1,41]},{6:$Vd,7:$Ve,8:$Vf,9:$Vg,10:$Vh,12:[1,42]},{6:$Vd,7:$Ve,8:$Vf,9:$Vg,10:$Vh,19:[1,43]},o($Vi,[2,10]),o($Vi,[2,11]),o($Vi,[2,12]),{4:44,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{6:$Vd,7:$Ve,8:$Vf,9:$Vg,10:$Vh,19:[1,45]},{4:46,7:$V0,11:$V1,13:$V2,14:$V3,15:$V4,16:$V5,17:$V6,18:$V7,20:$V8,21:$V9,22:$Va,23:$Vb,24:$Vc},{6:$Vd,7:$Ve,8:$Vf,9:$Vg,10:$Vh,12:[1,47]},o($Vi,[2,13])],
    defaultActions: {16:[2,1]},
    parseError: function parseError (str, hash) {
        if (hash.recoverable) {
            this.trace(str);
        } else {
            var error = new Error(str);
            error.hash = hash;
            throw error;
        }
    },
    parse: function parse(input) {
        var self = this, stack = [0], tstack = [], vstack = [null], lstack = [], table = this.table, yytext = '', yylineno = 0, yyleng = 0, recovering = 0, TERROR = 2, EOF = 1;
        var args = lstack.slice.call(arguments, 1);
        var lexer = Object.create(this.lexer);
        var sharedState = { yy: {} };
        for (var k in this.yy) {
            if (Object.prototype.hasOwnProperty.call(this.yy, k)) {
                sharedState.yy[k] = this.yy[k];
            }
        }
        lexer.setInput(input, sharedState.yy);
        sharedState.yy.lexer = lexer;
        sharedState.yy.parser = this;
        if (typeof lexer.yylloc == 'undefined') {
            lexer.yylloc = {};
        }
        var yyloc = lexer.yylloc;
        lstack.push(yyloc);
        var ranges = lexer.options && lexer.options.ranges;
        if (typeof sharedState.yy.parseError === 'function') {
            this.parseError = sharedState.yy.parseError;
        } else {
            this.parseError = Object.getPrototypeOf(this).parseError;
        }
        function popStack(n) {
            stack.length = stack.length - 2 * n;
            vstack.length = vstack.length - n;
            lstack.length = lstack.length - n;
        }
        _token_stack:
            var lex = function () {
                var token;
                token = lexer.lex() || EOF;
                if (typeof token !== 'number') {
                    token = self.symbols_[token] || token;
                }
                return token;
            };
        var symbol, preErrorSymbol, state, action, a, r, yyval = {}, p, len, newState, expected;
        while (true) {
            state = stack[stack.length - 1];
            if (this.defaultActions[state]) {
                action = this.defaultActions[state];
            } else {
                if (symbol === null || typeof symbol == 'undefined') {
                    symbol = lex();
                }
                action = table[state] && table[state][symbol];
            }
                        if (typeof action === 'undefined' || !action.length || !action[0]) {
                    var errStr = '';
                    expected = [];
                    for (p in table[state]) {
                        if (this.terminals_[p] && p > TERROR) {
                            expected.push('\'' + this.terminals_[p] + '\'');
                        }
                    }
                    if (lexer.showPosition) {
                        errStr = 'Parse error on line ' + (yylineno + 1) + ':\n' + lexer.showPosition() + '\nExpecting ' + expected.join(', ') + ', got \'' + (this.terminals_[symbol] || symbol) + '\'';
                    } else {
                        errStr = 'Parse error on line ' + (yylineno + 1) + ': Unexpected ' + (symbol == EOF ? 'end of input' : '\'' + (this.terminals_[symbol] || symbol) + '\'');
                    }
                    this.parseError(errStr, {
                        text: lexer.match,
                        token: this.terminals_[symbol] || symbol,
                        line: lexer.yylineno,
                        loc: yyloc,
                        expected: expected
                    });
                }
            if (action[0] instanceof Array && action.length > 1) {
                throw new Error('Parse Error: multiple actions possible at state: ' + state + ', token: ' + symbol);
            }
            switch (action[0]) {
            case 1:
                stack.push(symbol);
                vstack.push(lexer.yytext);
                lstack.push(lexer.yylloc);
                stack.push(action[1]);
                symbol = null;
                if (!preErrorSymbol) {
                    yyleng = lexer.yyleng;
                    yytext = lexer.yytext;
                    yylineno = lexer.yylineno;
                    yyloc = lexer.yylloc;
                    if (recovering > 0) {
                        recovering--;
                    }
                } else {
                    symbol = preErrorSymbol;
                    preErrorSymbol = null;
                }
                break;
            case 2:
                len = this.productions_[action[1]][1];
                yyval.$ = vstack[vstack.length - len];
                yyval._$ = {
                    first_line: lstack[lstack.length - (len || 1)].first_line,
                    last_line: lstack[lstack.length - 1].last_line,
                    first_column: lstack[lstack.length - (len || 1)].first_column,
                    last_column: lstack[lstack.length - 1].last_column
                };
                if (ranges) {
                    yyval._$.range = [
                        lstack[lstack.length - (len || 1)].range[0],
                        lstack[lstack.length - 1].range[1]
                    ];
                }
                r = this.performAction.apply(yyval, [
                    yytext,
                    yyleng,
                    yylineno,
                    sharedState.yy,
                    action[1],
                    vstack,
                    lstack
                ].concat(args));
                if (typeof r !== 'undefined') {
                    return r;
                }
                if (len) {
                    stack = stack.slice(0, -1 * len * 2);
                    vstack = vstack.slice(0, -1 * len);
                    lstack = lstack.slice(0, -1 * len);
                }
                stack.push(this.productions_[action[1]][0]);
                vstack.push(yyval.$);
                lstack.push(yyval._$);
                newState = table[stack[stack.length - 2]][stack[stack.length - 1]];
                stack.push(newState);
                break;
            case 3:
                return true;
            }
        }
        return true;
    }};
    /* generated by jison-lex 0.3.4 */
    var lexer = (function(){
    var lexer = ({
    
    EOF:1,
    
    parseError:function parseError(str, hash) {
            if (this.yy.parser) {
                this.yy.parser.parseError(str, hash);
            } else {
                throw new Error(str);
            }
        },
    
    // resets the lexer, sets new input
    setInput:function (input, yy) {
            this.yy = yy || this.yy || {};
            this._input = input;
            this._more = this._backtrack = this.done = false;
            this.yylineno = this.yyleng = 0;
            this.yytext = this.matched = this.match = '';
            this.conditionStack = ['INITIAL'];
            this.yylloc = {
                first_line: 1,
                first_column: 0,
                last_line: 1,
                last_column: 0
            };
            if (this.options.ranges) {
                this.yylloc.range = [0,0];
            }
            this.offset = 0;
            return this;
        },
    
    // consumes and returns one char from the input
    input:function () {
            var ch = this._input[0];
            this.yytext += ch;
            this.yyleng++;
            this.offset++;
            this.match += ch;
            this.matched += ch;
            var lines = ch.match(/(?:\r\n?|\n).*/g);
            if (lines) {
                this.yylineno++;
                this.yylloc.last_line++;
            } else {
                this.yylloc.last_column++;
            }
            if (this.options.ranges) {
                this.yylloc.range[1]++;
            }
    
            this._input = this._input.slice(1);
            return ch;
        },
    
    // unshifts one char (or a string) into the input
    unput:function (ch) {
            var len = ch.length;
            var lines = ch.split(/(?:\r\n?|\n)/g);
    
            this._input = ch + this._input;
            this.yytext = this.yytext.substr(0, this.yytext.length - len);
            //this.yyleng -= len;
            this.offset -= len;
            var oldLines = this.match.split(/(?:\r\n?|\n)/g);
            this.match = this.match.substr(0, this.match.length - 1);
            this.matched = this.matched.substr(0, this.matched.length - 1);
    
            if (lines.length - 1) {
                this.yylineno -= lines.length - 1;
            }
            var r = this.yylloc.range;
    
            this.yylloc = {
                first_line: this.yylloc.first_line,
                last_line: this.yylineno + 1,
                first_column: this.yylloc.first_column,
                last_column: lines ?
                    (lines.length === oldLines.length ? this.yylloc.first_column : 0)
                     + oldLines[oldLines.length - lines.length].length - lines[0].length :
                  this.yylloc.first_column - len
            };
    
            if (this.options.ranges) {
                this.yylloc.range = [r[0], r[0] + this.yyleng - len];
            }
            this.yyleng = this.yytext.length;
            return this;
        },
    
    // When called from action, caches matched text and appends it on next action
    more:function () {
            this._more = true;
            return this;
        },
    
    // When called from action, signals the lexer that this rule fails to match the input, so the next matching rule (regex) should be tested instead.
    reject:function () {
            if (this.options.backtrack_lexer) {
                this._backtrack = true;
            } else {
                return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. You can only invoke reject() in the lexer when the lexer is of the backtracking persuasion (options.backtrack_lexer = true).\n' + this.showPosition(), {
                    text: "",
                    token: null,
                    line: this.yylineno
                });
    
            }
            return this;
        },
    
    // retain first n characters of the match
    less:function (n) {
            this.unput(this.match.slice(n));
        },
    
    // displays already matched input, i.e. for error messages
    pastInput:function () {
            var past = this.matched.substr(0, this.matched.length - this.match.length);
            return (past.length > 20 ? '...':'') + past.substr(-20).replace(/\n/g, "");
        },
    
    // displays upcoming input, i.e. for error messages
    upcomingInput:function () {
            var next = this.match;
            if (next.length < 20) {
                next += this._input.substr(0, 20-next.length);
            }
            return (next.substr(0,20) + (next.length > 20 ? '...' : '')).replace(/\n/g, "");
        },
    
    // displays the character position where the lexing error occurred, i.e. for error messages
    showPosition:function () {
            var pre = this.pastInput();
            var c = new Array(pre.length + 1).join("-");
            return pre + this.upcomingInput() + "\n" + c + "^";
        },
    
    // test the lexed token: return FALSE when not a match, otherwise return token
    test_match:function(match, indexed_rule) {
            var token,
                lines,
                backup;
    
            if (this.options.backtrack_lexer) {
                // save context
                backup = {
                    yylineno: this.yylineno,
                    yylloc: {
                        first_line: this.yylloc.first_line,
                        last_line: this.last_line,
                        first_column: this.yylloc.first_column,
                        last_column: this.yylloc.last_column
                    },
                    yytext: this.yytext,
                    match: this.match,
                    matches: this.matches,
                    matched: this.matched,
                    yyleng: this.yyleng,
                    offset: this.offset,
                    _more: this._more,
                    _input: this._input,
                    yy: this.yy,
                    conditionStack: this.conditionStack.slice(0),
                    done: this.done
                };
                if (this.options.ranges) {
                    backup.yylloc.range = this.yylloc.range.slice(0);
                }
            }
    
            lines = match[0].match(/(?:\r\n?|\n).*/g);
            if (lines) {
                this.yylineno += lines.length;
            }
            this.yylloc = {
                first_line: this.yylloc.last_line,
                last_line: this.yylineno + 1,
                first_column: this.yylloc.last_column,
                last_column: lines ?
                             lines[lines.length - 1].length - lines[lines.length - 1].match(/\r?\n?/)[0].length :
                             this.yylloc.last_column + match[0].length
            };
            this.yytext += match[0];
            this.match += match[0];
            this.matches = match;
            this.yyleng = this.yytext.length;
            if (this.options.ranges) {
                this.yylloc.range = [this.offset, this.offset += this.yyleng];
            }
            this._more = false;
            this._backtrack = false;
            this._input = this._input.slice(match[0].length);
            this.matched += match[0];
            token = this.performAction.call(this, this.yy, this, indexed_rule, this.conditionStack[this.conditionStack.length - 1]);
            if (this.done && this._input) {
                this.done = false;
            }
            if (token) {
                return token;
            } else if (this._backtrack) {
                // recover context
                for (var k in backup) {
                    this[k] = backup[k];
                }
                return false; // rule action called reject() implying the next rule should be tested instead.
            }
            return false;
        },
    
    // return next match in input
    next:function () {
            if (this.done) {
                return this.EOF;
            }
            if (!this._input) {
                this.done = true;
            }
    
            var token,
                match,
                tempMatch,
                index;
            if (!this._more) {
                this.yytext = '';
                this.match = '';
            }
            var rules = this._currentRules();
            for (var i = 0; i < rules.length; i++) {
                tempMatch = this._input.match(this.rules[rules[i]]);
                if (tempMatch && (!match || tempMatch[0].length > match[0].length)) {
                    match = tempMatch;
                    index = i;
                    if (this.options.backtrack_lexer) {
                        token = this.test_match(tempMatch, rules[i]);
                        if (token !== false) {
                            return token;
                        } else if (this._backtrack) {
                            match = false;
                            continue; // rule action called reject() implying a rule MISmatch.
                        } else {
                            // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                            return false;
                        }
                    } else if (!this.options.flex) {
                        break;
                    }
                }
            }
            if (match) {
                token = this.test_match(match, rules[index]);
                if (token !== false) {
                    return token;
                }
                // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                return false;
            }
            if (this._input === "") {
                return this.EOF;
            } else {
                return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. Unrecognized text.\n' + this.showPosition(), {
                    text: "",
                    token: null,
                    line: this.yylineno
                });
            }
        },
    
    // return next match that has a token
    lex:function lex () {
            var r = this.next();
            if (r) {
                return r;
            } else {
                return this.lex();
            }
        },
    
    // activates a new lexer condition state (pushes the new lexer condition state onto the condition stack)
    begin:function begin (condition) {
            this.conditionStack.push(condition);
        },
    
    // pop the previously active lexer condition state off the condition stack
    popState:function popState () {
            var n = this.conditionStack.length - 1;
            if (n > 0) {
                return this.conditionStack.pop();
            } else {
                return this.conditionStack[0];
            }
        },
    
    // produce the lexer rule set which is active for the currently active lexer condition state
    _currentRules:function _currentRules () {
            if (this.conditionStack.length && this.conditionStack[this.conditionStack.length - 1]) {
                return this.conditions[this.conditionStack[this.conditionStack.length - 1]].rules;
            } else {
                return this.conditions["INITIAL"].rules;
            }
        },
    
    // return the currently active lexer condition state; when an index argument is provided it produces the N-th previous condition state, if available
    topState:function topState (n) {
            n = this.conditionStack.length - 1 - Math.abs(n || 0);
            if (n >= 0) {
                return this.conditionStack[n];
            } else {
                return "INITIAL";
            }
        },
    
    // alias for begin(condition)
    pushState:function pushState (condition) {
            this.begin(condition);
        },
    
    // return the number of states currently on the stack
    stateStackSize:function stateStackSize() {
            return this.conditionStack.length;
        },
    options: {},
    performAction: function anonymous(yy,yy_,$avoiding_name_collisions,YY_START) {
    var YYSTATE=YY_START;
    switch($avoiding_name_collisions) {
    case 0:/* skip whitespace */
    break;
    case 1:return 20
    break;
    case 2:yy_.yytext = yy_.yytext.slice(1,-1);     return 15
    break;
    case 3:return 8
    break;
    case 4:return 9
    break;
    case 5:return 19
    break;
    case 6:return 7
    break;
    case 7:return 6
    break;
    case 8:return 10
    break;
    case 9:return 11
    break;
    case 10:return 12
    break;
    case 11:return 24
    break;
    case 12:return 22
    break;
    case 13:return 13
    break;
    case 14:return 21
    break;
    case 15:return 23
    break;
    case 16:return 14
    break;
    case 17:return 18
    break;
    case 18:return 16
    break;
    case 19:return 17
    break;
    case 20:return 5
    break;
    case 21:return 'INVALID'
    break;
    }
    },
    rules: [/^(?:\s+)/,/^(?:[0-9]+(\.[0-9]+)?\b)/,/^(?:'([+-\/|\/%*$\^!" <>=.a-zA-Z0-9_{}&\?\[\]]*)')/,/^(?:\*)/,/^(?:\/)/,/^(?:,)/,/^(?:-)/,/^(?:\+)/,/^(?:\^)/,/^(?:\()/,/^(?:\))/,/^(?:PI\b)/,/^(?:true\b)/,/^(?:now\b)/,/^(?:false\b)/,/^(?:E\b)/,/^(?:jspath\b)/,/^(?:getrefval\b)/,/^(?:loadrest\b)/,/^(?:img_attr\b)/,/^(?:$)/,/^(?:.)/],
    conditions: {"INITIAL":{"rules":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21],"inclusive":true}}
    });
    return lexer;
    })();
    parser.lexer = lexer;
    function Parser () {
      this.yy = {};
    }
    Parser.prototype = parser;parser.Parser = Parser;
    return new Parser;
    })();
    
    
    if (typeof require !== 'undefined' && typeof exports !== 'undefined') {
    exports.parser = parser;
    exports.Parser = parser.Parser;
    exports.parse = function () { return parser.parse.apply(parser, arguments); };
    exports.main = function commonjsMain (args) {
        if (!args[1]) {
            console.log('Usage: '+args[0]+' FILE');
            process.exit(1);
        }
 //       var source = require('fs').readFileSync(require('path').normalize(args[1]), "utf8");
//        var source = require(fs).readFileSync(require(path).normalize(args[1]), "utf8");
//        var source = require('fs-extra').readFileSync(require('path').normalize(args[1]), "utf8");

        return exports.parser.parse(source);
    };
    if (typeof module !== 'undefined' && require.main === module) {
      exports.main(process.argv.slice(1));
    }
    }

//export default fieldcalc {
//    export default fieldcalc {
// export default fieldcalc {

// export function fieldcalc() {
//    install: (app, options) => {

//var fieldcalc = (function () {    
var fieldcalc = (function () { 

    var cdoc;
    var server_props;
    var geo_table;
    var calclist={};
    var readycallback;
    var refvals={};
    var sparser;
    var sjspath;
    var instancecounter=0;
    var curentinstance;
    var docs={};
    
    var cfieldname='';
    var cexpression='';
    
    var cdate=function(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
    
        return yyyy + '-' + mm + '-'+dd;
    }
//console.log(cdate());    
    var getrefval=function(dataset_id, id, fieldname){
        //console.log('getrefval', fieldname, id);
        var doc=cdoc;//Сохранение ссылки на текущий документ в контексе вызова функции cdoc может поменяться.
        var curinstance = curentinstance;
        if(id==null || id==undefined)
            return '';
        if(typeof id==='object' && id.id)
            id=id.id;
        var strReturn = "";
        if(id===undefined || id=='' ||  isNaN(parseInt(id)))
            return '';
        var key=fieldname+dataset_id + id;
        if (key in refvals && refvals[key]!=''){
            return refvals[key];
        }
        // else if(key in refvals && refvals[key]==''){
        // 	throw new UserException("calculating async");
        // }
        if(server_props === undefined){
        
    // все участки жквери переписать на вью -т.е. в данном случае запилить аксиос вместо $.ajax
    // подключить библиотеку jspath
/*
window.$axios
             .get("https://isling.org:8000"+
*/

            //axios
            window.$axios
//            .get('/dataset/list?f='+dataset_id+'&iDisplayStart=0&iDisplayLength=100&s_fields='+fieldname+'&f_id='+id)
            .get(window.$store.state.mainurl+'/dataset/list?f='+dataset_id+'&iDisplayStart=0&iDisplayLength=100&s_fields='+fieldname+'&f_id='+id)
            .then((response) => {
 //               obj = JSON.parse(response.data);
               var obj = response.data;

console.log(response);  

                  if(obj.aaData.length==1){
                    strReturn = obj.aaData[0][fieldname];
                  }
                  else  {
                    strReturn = "";
                  }
                  refvals[key]=strReturn;
                  iteratefields( curinstance);
                  
                })
                .catch(error => console.log(error));
                // непонятная строка
//                throw new UserException("calculating async");
                throw new Error("calculating async");

//             $.ajax({
//                 url: '/dataset/list?f='+dataset_id+'&iDisplayStart=0&iDisplayLength=100&s_fields='+fieldname+'&f_id='+id,
//                 success: function(result) {
//                   obj = JSON.parse(result);
//                   if(obj.aaData.length==1){
//                       strReturn = obj.aaData[0][fieldname];
//                   }
//                   else
//                       strReturn = "";
                  
//                   refvals[key]=strReturn;
//                   iteratefields( curinstance);
//                 },
//                 async:true
// // непонятная строка 
//             });
//             throw new UserException("calculating async");
// // непонятная строка 
        }
        else{
            server_props.meta_manager.get_table_json({}, dataset_id, function (meta){
                var g_table = new geo_table(server_props.user_id, dataset_id,  meta);
                qparams={f_id:id+'-PropertyIsEqualTo'};
                g_table.documentquery(server_props.req, [fieldname],qparams, '', function(error, result){
                    //console.log('get fer val result',result);
                    if(error){
                        refvals[key]=error;
                    }
                    else if(result.aaData.length==1){
                        strReturn=result.aaData[0][fieldname];
                        refvals[key]=strReturn;
                    }
                    else{
                        refvals[key]="Multiple value";
                    }
                    iteratefields( curinstance);
                });
            }, server_props.req);
            throw new Error("calculating async");
            
        }
    }
    
    
    var loadrest=function(url){
//        console.log('loadrest', url);
        var doc=cdoc;//Сохранение ссылки на текущий документ в контексе вызова функции cdoc может поменяться.
        var curinstance = curentinstance;
        var fieldname = cfieldname;
        
        // var key=url;
        if (fieldname in curinstance.calclist  && curinstance.calclist[fieldname].value!==undefined){
            if( curinstance.calclist[fieldname].value=='---proc---' )
                throw new Error("calculating async");
            else
                return curinstance.calclist[fieldname].value;
        }
        curinstance.calclist[fieldname].value='---proc---';
        // else if(key in refvals && refvals[key]==''){
        // 	throw new UserException("calculating async");
        // }
        if(server_props === undefined){

           window.$axios
            //.get("https://isling.org:8000"+url)
            .get(window.$store.state.mainurl+url)
            .then((response) => {
//                result=JSON.parse(response);
                curinstance.calclist[fieldname].value=response.data.data;
                iteratefields( curinstance);
              })
              .catch(error => console.log(error));

              let test = "https://isling.org:8000"+url;
              let test2 = window.$store.state.mainurl+url;
              console.log(test, test2);
              // ???
              iteratefields( curinstance);
//             $.get({
//                 url: url,
//                 success: function(result) {
//                     result=JSON.parse(result);
//                   curinstance.calclist[fieldname].value=result.data;
//                   iteratefields( curinstance);
//                 },
//                 async:true
//             }).fail(function() {
// //
//                 curinstance.calclist[fieldname].value="jquery get fail";
//                 iteratefields( curinstance);
// // непонятные строки - а как сдлать так же в аксиосе?
//             })
            
        }
        else{
            // const https = require('https');
//            console.log('loadrest calling', url);
            var parts=url.split('?');
            url=parts[0];
            var params=[];
            var queryvalues={};
            if(parts.length>1){
                params=parts[1].split('&');
                for(var i=0; i<params.length; i++){
                    kv=params[i].split('=');
                    queryvalues[kv[0]]=kv[1];
                }
            }
            if(url in global.customhandlers){
                global.customhandlers[url](queryvalues, function(error, data){
                    if(error){
                        curinstance.calclist[fieldname].value=error;
                    }
                    else if (data!==undefined){
                        curinstance.calclist[fieldname].value=data;
                    }
                    else{
                        curinstance.calclist[fieldname].value='handler returns wrong value';
                    }
                        
                    iteratefields( curinstance);
                })
            }
            else{
                return url + ' in global.customhandlers not found';
                // iteratefields( curinstance);
            }
            // var request = require('request');
    
            // if(url.indexOf('http')==-1){
            // 	var config = require("../../../conf.json");
            // 	servername=config.server.url;
            // 	// if(config.server.port!=80)
            // 	// 	servername=servername+":"+config.server.port;
            // 	url=servername+url;
            // }
    
            // request(url, function (error, response, body) {
            // 	if(error){
            // 		refvals[key]=error;
            // 		iteratefields(doc);
            // 	}
            // 	else{
            // 		refvals[key]=body;
            // 		iteratefields(doc);
            // 	}
            // });
        }
        throw new Error("calculating async");
    }
    
    var getdocvalue=function(path){
        //console.log('getdocvalue',path);
//        if(sjspath)
            var res = window.jspath.apply(path, cdoc);
//        else
//            res= JSPath.apply(path, cdoc);
        //console.log('getdocvalue res=',res);
        if(Array.isArray(res)){
            if(res.length>0){
                if(typeof res[0]=='number')
                    return res[0]+'';
                else if(typeof res[0] == "object" && res[0].id) {
                    return res[0].id+'';
                }
                else
                    return res[0];
            }
            else
                return '';
        }
        else
            return res;
    }
        
    function parsevalue(fieldname, instance){
        //console.log('calculatefield',expression);
        var doc=instance.doc;
        var expression = instance.calclist[fieldname].expression;
        if(expression=='')
            doc[fieldname] = '';
        if(expression.charAt(0)=='='){
            //console.log(expression);
            cdoc=doc;
            cfieldname=fieldname;
            cexpression=expression;
            try{
//                if(parser)
                    doc[fieldname] = parser.parse(expression.substr(1));
//                else
//                    doc[fieldname] = sparser.parse(expression.substr(1));
                delete instance.calclist[fieldname];
            }
            catch(e){
                 console.log(e)
                // calclist[meta.columns[i].fieldname] = expression.substr(1);
            }
        }
        else if(expression.toUpperCase()=='EMPTY' || expression.toUpperCase()=='NULL'){
            doc[fieldname] = '';
            delete instance.calclist[fieldname];
        }
        else{
            doc[fieldname] = expression;
            delete instance.calclist[fieldname];
        }
    }
    
    function iteratefields( instance){
        //console.log('iteratefields',doc);
        curentinstance = instance;
        
    
        for (var fieldname in instance.calclist) {
            if (instance.calclist.hasOwnProperty(fieldname)) {
                // try{
                    //console.log('iteratefields fieldname',fieldname, calclist[fieldname])
                    parsevalue(fieldname, instance);
                    //console.log('iteratefields value',fieldname, doc[fieldname])
                // 	delete calclist[fieldname];
                // }
                // catch(e){
                // 	//calclist[fieldname]=null;
                // }
            }
        }
        var cnt=Object.keys(instance.calclist).length;	
        //console.log('iteratefields cnt',cnt);
        if(cnt==0 && instance.callback){
            instance.callback('', instance.doc);
            instance.callback=undefined;
        }
        
    }
    
    var calculatefields = function (meta, doc, mode, server, ready) {
        //console.log('calculatefields',doc);
        readycallback=ready;
        instancecounter++;
        curentinstance = instancecounter;
        var instance={
            doc:doc,
            callback:ready,
            calclist:{}
        };
        docs['d_'+curentinstance]=instance;
    
        if(server!=undefined){		
//            geo_table = require("../../../controllers/dataset/geo_table"); // ????????
//            sparser = require("./parser");
            sparser.fieldcalc = fieldcalc;
//            sjspath = require("jspath");
            server_props=server;
/// здесь надо все переделать тоже? типа?:
sparser = parser;
sjspath = jspath;

        }
        if(mode==='copy'){
            for(var i=0; i<meta.columns.length; i++) {
                if(meta.columns[i].copy_value && meta.columns[i].copy_value!=''){
                        instance.calclist[meta.columns[i].fieldname]={expression: meta.columns[i].copy_value};
                        // parsevalue(meta.columns[i].fieldname, meta.columns[i].copy_value, instance);
                }
            }		
        }
        if(mode==='form'){
            for(var i=0; i<meta.columns.length; i++) {
                if(server==undefined && meta.columns[i].init_value && meta.columns[i].init_value!=''){
                    if(meta.columns[i].fieldname=='published' || meta.columns[i].tablename!='main')
                        continue;
                    if(doc[meta.columns[i].fieldname]==undefined || doc[meta.columns[i].fieldname]=='NULL' || doc[meta.columns[i].fieldname]=='')
                        instance.calclist[meta.columns[i].fieldname]={expression: meta.columns[i].init_value};					
                }
            }
        }
        if(mode==='view'){
            for(var i=0; i<meta.columns.length; i++) {
                if(server==undefined && meta.columns[i].init_value && meta.columns[i].init_value!='' && (meta.columns[i].widget && meta.columns[i].widget.name=='calculated')){
                    if(meta.columns[i].fieldname=='published' || meta.columns[i].tablename!='main')
                        continue;
                    if(doc[meta.columns[i].fieldname]==undefined || doc[meta.columns[i].fieldname]=='NULL' || doc[meta.columns[i].fieldname]=='')
                        instance.calclist[meta.columns[i].fieldname]={expression: meta.columns[i].init_value};					
                }
            }
        }
        else{
            for(var i=0; i<meta.columns.length; i++) {
                if(meta.columns[i].fieldname in doc && meta.columns[i].default_value && meta.columns[i].default_value!=''){
                    if(meta.columns[i].fieldname=='published' || meta.columns[i].tablename!='main')
                        continue;
                    if(doc[meta.columns[i].fieldname]==undefined || doc[meta.columns[i].fieldname]==null || doc[meta.columns[i].fieldname]=='NULL' || doc[meta.columns[i].fieldname]=='' || (meta.columns[i].widget && meta.columns[i].widget.name=='calculated'))
                        instance.calclist[meta.columns[i].fieldname]={expression: meta.columns[i].default_value};
    
                    // parsevalue(meta.columns[i].fieldname, meta.columns[i].default_value, instance)
                        
                }
    
            }
        }
        iteratefields(instance);	
    }
    
    var synccalculate = function (doc, fieldname,expression, server) {
        //console.log('calculatefields',doc);
        // readycallback=ready;
        instancecounter++;
        curentinstance = instancecounter;
        var instance={
            doc:doc,
            // callback:ready,
            calclist:{}
        };
        docs['d_'+curentinstance]=instance;
    
        if(server!=undefined){		
//            geo_table = require("../../../controllers/dataset/geo_table");
sparser.fieldcalc = fieldcalc;
//            sjspath = require("jspath");
server_props=server;
/// здесь надо все переделать тоже? типа?:
sparser = parser;
sjspath = jspath;

        }
        
            
        instance.calclist[fieldname]={expression: expression};
        parsevalue(fieldname, instance);
    
                    // parsevalue(meta.columns[i].fieldname, meta.columns[i].default_value, instance)
                        
            
        
    }
    return {
        calculatefields: calculatefields, 
        parsevalue:parsevalue,
        getdocvalue	:getdocvalue,
        getrefval: getrefval,
        loadrest: loadrest,
        synccalculate: synccalculate,
        cdate: cdate,
      }
    
})();
///// parser //////

///// end parser /////






export {fieldcalc};

//});

//};

    // if(typeof (module)!== 'undefined'){
    //     module.exports = fieldcalc;
    //     global.fieldcalc = fieldcalc;
    // }
    
//    };

 //export default {fieldcalc};

 //export {fieldcalc};
